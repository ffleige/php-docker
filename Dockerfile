#
# PHP 7.4 cli for use as remote interpreter within JetBrains PHPStorm
#
FROM php:7.4-cli

#
# Install required dependencies for additional php modules
#
RUN apt-get clean && apt-get update && apt-get install --fix-missing -y \
  libxml2-dev \
  bison \
  libssl-dev \
  libcurl4-openssl-dev \
  libzip-dev \
  pkg-config \
  locales \
  zip \
  unzip
 
#
# Setup locale
#
RUN echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
RUN echo 'de_DE.UTF-8 UTF-8' >> /etc/locale.gen
RUN locale-gen

#
# Install additional php modules
#
RUN docker-php-ext-install mysqli pdo_mysql dom json soap xmlwriter curl opcache gettext intl zip

#
# Install additional PECL modules
#
RUN pecl channel-update pecl.php.net
RUN pecl install apcu && docker-php-ext-enable apcu
RUN pecl install redis && docker-php-ext-enable redis

#
# config php modules
#
RUN echo 'opcache.enable=1' >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.enable_cli=1' >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.interned_strings_buffer=8' >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.max_accelerated_files=10000' >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.memory_consumption=128' >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.save_comments=1' >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.revalidate_freq=1' >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

#
# install xdebug extension
#
RUN yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

#
# configurations for xdebug profiler
# - profiles will be written to directory _profiler of the current project
# - files will ne named "trace.SCRIPTNAME_TIMESTAMP.xt"
#
RUN echo 'xdebug.profiler_output_dir=/opt/project/_profiler' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo 'xdebug.trace_output_name=trace.%s_%t' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

#
# show errors to console
#
RUN echo 'display_errors = STDOUT' >> /usr/local/etc/php/php.ini
